"""serialbox URL Configuration
"""
from django.urls import include, re_path
from rest_framework import urls as rf_urls
from serialbox.api import urls as api_urls

urlpatterns = [
    re_path(r'^', include(api_urls)),
    re_path(r'^auth/', include(rf_urls, namespace='rest_framework')),
]
